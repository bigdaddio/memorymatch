﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour 
{
    public Sprite FrontSprite;
    public Sprite BackSprite;
    public AudioClip MatchSound;
    public GameObject matchParticles;
    private GameController gameController;

	// Use this for initialization
	void Start ()
	{
	    GameObject gc = GameObject.Find("GameControl");
	    if (gc != null)
	    {
	        gameController = gc.GetComponent<GameController>();
	    }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown()
    {
        if (GetComponent<SpriteRenderer>().sprite == BackSprite)
        {
            GetComponent<Animation>().Play();
            gameController.CardSelected(gameObject);
        }
    }

    void SwapSprite()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();

        if (spriteRenderer.sprite == BackSprite)
            spriteRenderer.sprite = FrontSprite;
        else
            spriteRenderer.sprite = BackSprite;
    }

    public void KillCard()
    {
        if (GetComponent<SpriteRenderer>().sprite == BackSprite)
            GetComponent<Animation>().Play("FlipFade");
        else
            GetComponent<Animation>().PlayQueued("JustFade");

        GameObject particles = (GameObject)Instantiate(matchParticles, transform.position, Quaternion.identity);

        StartCoroutine(doWait(1.5f));
    }

    IEnumerator doWait(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(gameObject);
    }
}
