﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[System.Serializable]

public class GameTile : IComparable<GameTile>
{
    public float SortKey;
    public Sprite FrontSprite;
    public AudioClip MatchSound;

    public int CompareTo(GameTile compareTile)
    {
        if (compareTile == null)
            return 1;

        else
            return this.SortKey.CompareTo(compareTile.SortKey);
    }

    public void GenerateKey ()
    {
        SortKey = Random.Range(0, 100);
    }
}
