﻿using UnityEngine;
using System.Collections;

public class GameWin : MonoBehaviour {

    private GameController gameController;

	// Use this for initialization
	void Start () {
        GameObject gc = GameObject.Find("GameControl");
        gameController = gc.GetComponent<GameController>();
    }
	
	public void resetGame()
    {
        gameController.GameReset();
        Destroy(transform.parent.gameObject);
    }
}
