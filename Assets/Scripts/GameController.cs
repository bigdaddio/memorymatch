﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    public GameObject cardOne;
    public GameObject cardTwo;
    public GameObject WinAnimation;
    public TileShuffle tileObject;
    public int guiFadeSpeed = 1;
    public AudioClip FlipSound;
    public AudioClip MatchSound;
    public AudioClip WinSound;
    public AudioClip NewGameSound;
    private GameObject guiCanvas;
    private int winMatches;
    private int matches;
    private AudioSource audioSource;

    void Awake ()
    {
        guiCanvas = GameObject.Find("MainGui");
        audioSource = GetComponent<AudioSource>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CardSelected(GameObject theCard)
    {
        if (cardOne == null)
        {
            audioSource.PlayOneShot(FlipSound);
            cardOne = theCard;
            return;
        }

        if (cardTwo == null)
        {
            audioSource.PlayOneShot(FlipSound);
            cardTwo = theCard;
            checkMatch();
            return;
        }

        if (cardTwo != null)
        {
            audioSource.PlayOneShot(FlipSound);
            cardOne.GetComponent<Animation>().PlayQueued("FlipTile");
            cardTwo.GetComponent<Animation>().PlayQueued("FlipTile");
            cardTwo = null;
            cardOne = theCard;
            return;
        }
    }

    bool checkMatch()
    {
        if (cardOne.GetComponent<Tile>().FrontSprite == cardTwo.GetComponent<Tile>().FrontSprite)
        {
            AudioClip matchSound = cardOne.GetComponent<Tile>().MatchSound;
            audioSource.PlayOneShot(matchSound);
            cardOne.GetComponent<Tile>().KillCard();
            cardTwo.GetComponent<Tile>().KillCard();
            cardOne = null;
            cardTwo = null;
            matches += 1;

            if (matches == winMatches)
                StartCoroutine(DoWin(1.0f));

            return true;
        }
        return false;
    }

    public void GameEasy()
    {
        StartCoroutine(FadeOutGui(guiFadeSpeed));
        tileObject.Board8();
        matches = 0;
        winMatches = 4;
    }

    public void GameNormal()
    {
        StartCoroutine(FadeOutGui(guiFadeSpeed));
        tileObject.Board12();
        matches = 0;
        winMatches = 6;
    }

    public void GameHard()
    {
        StartCoroutine(FadeOutGui(guiFadeSpeed));
        tileObject.Board20();
        matches = 0;
        winMatches = 10;
    }

    public void GameReset()
    {
        StartCoroutine(FadeInGui(guiFadeSpeed));
    }

    public IEnumerator FadeOutGui(float speed)
    {
        CanvasGroup canvasGroup = guiCanvas.GetComponent<CanvasGroup>();
        while (canvasGroup.alpha > 0f)
        {
            canvasGroup.alpha -= speed * Time.deltaTime;
            yield return null;
        }
        guiCanvas.SetActive(false);
        audioSource.PlayOneShot(NewGameSound);
    }

    public IEnumerator FadeInGui(float speed)
    {
        CanvasGroup canvasGroup = guiCanvas.GetComponent<CanvasGroup>();
        guiCanvas.SetActive(true);
        while (canvasGroup.alpha < 1f)
        {
            canvasGroup.alpha += speed * Time.deltaTime;
            yield return null;
        }
    }

    public IEnumerator DoWin(float delay)
    {
        yield return new WaitForSeconds(delay);
        Instantiate(WinAnimation);
        audioSource.PlayOneShot(WinSound);
    }

}
