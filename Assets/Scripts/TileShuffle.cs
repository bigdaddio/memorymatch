﻿using UnityEngine;
using System.Collections.Generic;

public class TileShuffle : MonoBehaviour
{
    public GameObject TilePrefab;
    public List<TileData> TileSprites;
    public Sprite TileBack;
    private GameObject[] TileGameObject;
    public List<GameTile> gameTiles;
    private float[] TileX;
    private float[] TileY;

    void Awake()
    {
        TileGameObject = new GameObject[20];
    }
	// Use this for initialization
	void Start () {
	
	}

    private void GenTiles(int tileCount)
    {
        gameTiles = new List<GameTile>();
        int modValue = tileCount / 2;
        for (int i = 0; i < tileCount; i++)
        {
            GameTile gt = new GameTile();
            gt.GenerateKey();
            gt.FrontSprite = TileSprites[i % modValue].TileSprite;
            gt.MatchSound = TileSprites[i % modValue].MatchSound;
            gameTiles.Add(gt);
        }
        gameTiles.Sort();
    }


    public void Board8()
    {
        GenTiles(8);

        TileX = new[] { -3f, -1.0f, 1.0f, 3f };
        TileY = new[] { 1.0f, -1.0f };
        int index = 0;
        Vector3 tilePos = Vector3.zero;
        Vector3 tileScale = new Vector3(1.5f, 1.5f, 1.0f);

        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                TileGameObject[index] = Instantiate(TilePrefab);
                tilePos.x = TileX[j];
                tilePos.y = TileY[i];
                TileGameObject[index].transform.position = tilePos;
                TileGameObject[index].transform.localScale = tileScale;
                Tile tile = TileGameObject[index].GetComponent<Tile>();
                tile.FrontSprite = gameTiles[index].FrontSprite;
                tile.MatchSound = gameTiles[index].MatchSound;
                tile.BackSprite = TileBack;
                index++;
            }
        }
    }

    public void Board12()
    {
        GenTiles(12);

        TileX = new[] { -3f, -1.0f, 1.0f, 3f };
        TileY = new[] { 2.0f, 0.0f, -2.0f };
        int index = 0;
        Vector3 tilePos = Vector3.zero;
        Vector3 tileScale = new Vector3(1.5f, 1.5f, 1.0f);

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                TileGameObject[index] = Instantiate(TilePrefab);
                tilePos.x = TileX[j];
                tilePos.y = TileY[i];
                TileGameObject[index].transform.position = tilePos;
                TileGameObject[index].transform.localScale = tileScale;
                Tile tile = TileGameObject[index].GetComponent<Tile>();
                tile.FrontSprite = gameTiles[index].FrontSprite;
                tile.MatchSound = gameTiles[index].MatchSound;
                tile.BackSprite = TileBack;
                index++;
            }
        }
    }

    public void Board20()
    {
        GenTiles(20);

        TileX = new[] { -3f, -1.5f, 0f, 1.5f, 3f };
        TileY = new[] { 2.5f, 1.0f, -0.5f, -2.0f };
        int index = 0;
        Vector3 tilePos = Vector3.zero;
        Vector3 tileScale = new Vector3(1.25f, 1.25f, 1.0f);
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                TileGameObject[index] = Instantiate(TilePrefab);
                tilePos.x = TileX[j];
                tilePos.y = TileY[i];
                TileGameObject[index].transform.position = tilePos;
                TileGameObject[index].transform.localScale = tileScale;
                Tile tile = TileGameObject[index].GetComponent<Tile>();
                tile.FrontSprite = gameTiles[index].FrontSprite;
                tile.MatchSound = gameTiles[index].MatchSound;
                tile.BackSprite = TileBack;
                index++;
            }
        }
    }


	
}
