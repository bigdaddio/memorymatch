﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TileData {

    public Sprite TileSprite;
    public AudioClip MatchSound;
}
